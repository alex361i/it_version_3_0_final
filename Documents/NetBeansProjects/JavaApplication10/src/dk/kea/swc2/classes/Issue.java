/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.classes;

import java.io.FileNotFoundException;
import java.io.Serializable;

/**
 *
 * @author yo
 */
public class Issue implements Serializable{
    
    private  String subject;
    private  String priority;
    private  String status;
    private  String comment;
    
    public Issue() {
        this(" ","","","");
    }
    public Issue(String subject, String priority, String status,String comment) {
        this.subject = subject;
        this.priority = priority;
        this.status = status;
        this.comment=comment;
        

    }

    
    
    public String getSubject() {
        return subject;
    }
 
    public void setSubject(String subject) {
        this.subject=subject;
    }

    public String SubjectProperty() {
        return subject;
    }
 
    public String getPriority() {
        return priority;
    }
    
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int PriorityProperty(int Priority) {
        return Priority;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int StatusProperty(int Status) {
        return Status;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public String CommentProperty(String comment) {
        return comment;
    }

    @Override
    public String toString() {
        return  subject +"  "+  status +"  "+comment+ "  "+ priority   ;
    }
    
}


