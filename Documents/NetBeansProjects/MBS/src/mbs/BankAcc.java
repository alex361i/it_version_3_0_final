/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mbs;

/**
 *
 * @author yo
 */
public class BankAcc
 {  
    
    public BankAcc()
    {  
       balance = 0;
    }
 
    
    public BankAcc(double iniBalance)
    {  
       balance = iniBalance;
   }
 
    
      
    
    public void deposit(double amount) 
    {  
       balance = balance + amount;
    }
 
  
    public void withdraw(double amount) 
    {  
       balance = balance - amount;
    }
 
    
    public double getBalance()
    {  
       return balance; 
    }
    
   
    public void transfer(double amount, BankAcc diferent)
   {  
       withdraw(amount);
       diferent.deposit(amount);
    }
   
    public String toString()
    {
       return "the balance is : " + balance ;
    }
 
    private double balance; 
 }
