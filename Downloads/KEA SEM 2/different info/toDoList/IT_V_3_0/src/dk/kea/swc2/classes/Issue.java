/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.classes;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author yo
 */
public class Issue
{
    
    String subject;
    String priority;
    String status;
    String comment;
    
    public Issue() {
        this(" ","","","");
    }
    public Issue(String subject, String priority, String status,String comment) {
        this.subject = subject;
        this.priority = priority;
        this.status = status;
        this.comment=comment;
    }

    ObjectOutputStream output;
    ObjectInputStream   input;
    
    public String getSubject() {
        return subject;
    }
 
    public void setSubject(String subject) {
        this.subject=subject;
    }

    public String getPriority() {
        return priority;
    }
    
    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    @Override
    public String toString() {
        return  subject + " " + priority + " "+ status + " " + comment  ;
    }
    
}


